chopsuey is (going to become) a collection of all my little C utility
routines and types, helpers and supposed one-offs that don't belong
anywhere else in particular.  The kind of stuff I whipped up one too
many times or copied and pasted into one too many a project.  Still
these things aren't big enough on their own (yet?) to warrant
extracting them into their own projects, with all the hassle and
overhead that brings (as if I would launch e.g. libbithacks now for
this one-liner here or even libmemory for that allocator there, asf).
But code spreads and grows rampant, and eventually I had this
ingenious idea to put it all into *one single* library dedicated to,
um, unspecifics, comprising all those ... assorted pieces and be done
with it!

chopsuey should work with any standard-compliant C99 implementation.

The GNU Autotools are used to configure, build, test, and install
chopsuey.

Note: I've abandoned the previous, C++(-only) version of chopsuey some
time ago already (for various reasons I won't go into here), but the
code is still available in the abandoned-cpp branch.

--oe, Jul 2022
