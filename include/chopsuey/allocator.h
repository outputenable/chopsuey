/*
 * SPDX-FileCopyrightText: 2021 Oliver Ebert <oe@outputenable.net>
 *
 * SPDX-License-Identifier: Zlib
 */

/**
 ** @file
 ** Allocator facilities.
 **
 ** @see
 **   WG14 <a href="https://wg14.link/n1085">N1085: Proposal to augment the
 **   interface of malloc/free/realloc/calloc</a> by Howard Hinnant
 **/

#ifndef CHOPSUEY_ALLOCATOR_H
#define CHOPSUEY_ALLOCATOR_H 1

#include <stddef.h>

#include "compat.h"

#include "hedley.h"

/// @cond
HEDLEY_BEGIN_C_DECLS
/// @endcond

struct ChopsueyAllocatorOps;

typedef struct ChopsueyAllocator_
	{
	struct ChopsueyAllocatorOps const *ops;
	void *ctx;
	}
	ChopsueyAllocator;

struct ChopsueyAllocatorOps
	{
	HEDLEY_WARN_UNUSED_RESULT
	void *(*malloc)(size_t size, void *ctx);

	void (*free)(void const *ptr , void *ctx);

	HEDLEY_WARN_UNUSED_RESULT
	void *(*calloc)(size_t nmemb, size_t size, void *ctx);

	void *(*realloc)(void *ptr, size_t size, void *ctx);

	void (*ref)(ChopsueyAllocator alloc);

	void (*unref)(ChopsueyAllocator alloc);
	};

HEDLEY_WARN_UNUSED_RESULT
HEDLEY_NON_NULL(1)
static inline ChopsueyAllocator
chopsuey_alloc_create(
	struct ChopsueyAllocatorOps const *const ops,
	void *const ctx)
	{
	return CHOPSUEY_LITERAL(, ChopsueyAllocator){ ops, ctx };
	}

HEDLEY_WARN_UNUSED_RESULT
ChopsueyAllocator chopsuey_system_allocator(void);

static inline void
chopsuey_alloc_ref(ChopsueyAllocator const alloc)
	{
	alloc.ops->ref(alloc);
	}

static inline void
chopsuey_alloc_unref(ChopsueyAllocator const alloc)
	{
	alloc.ops->unref(alloc);
	}

HEDLEY_WARN_UNUSED_RESULT
HEDLEY_MALLOC
static inline void *
chopsuey_malloc(size_t const size, ChopsueyAllocator const alloc)
	{
	return alloc.ops->malloc(size, alloc.ctx);
	}

static inline void
chopsuey_free(void const *const ptr, ChopsueyAllocator const alloc)
	{
	alloc.ops->free(ptr, alloc.ctx);
	}

HEDLEY_WARN_UNUSED_RESULT
static inline void *
chopsuey_calloc(
	size_t const nmemb,
	size_t const size,
	ChopsueyAllocator const alloc)
	{
	return alloc.ops->calloc(nmemb, size, alloc.ctx);
	}

static inline void *
chopsuey_realloc(
	void *const ptr,
	size_t const size,
	ChopsueyAllocator const alloc)
	{
	return alloc.ops->realloc(ptr, size, alloc.ctx);
	}

HEDLEY_WARN_UNUSED_RESULT
HEDLEY_NON_NULL(2)
void *chopsuey_overalloc_pow2(
	void *ptr,
	size_t *pnmemb,
	size_t size,
	ChopsueyAllocator alloc);

#ifndef chopsuey_overalloc
#   define chopsuey_overalloc chopsuey_overalloc_pow2
#endif

/// @cond
HEDLEY_END_C_DECLS
/// @endcond

#endif /* CHOPSUEY_ALLOCATOR_H */
