/*
 * SPDX-FileCopyrightText: © 2022 Oliver Ebert <oe@outputenable.net>
 *
 * SPDX-License-Identifier: Zlib
 */

/**
 ** @file
 ** Runtime checks.
 **/
#ifndef CHOPSUEY_CHECK_H
#define CHOPSUEY_CHECK_H 1

#include "hedley.h"

/// @cond
HEDLEY_BEGIN_C_DECLS
/// @endcond

#define CHOPSUEY_CHECK(expr) do                                              \
    if (HEDLEY_UNLIKELY( !(expr)))                                           \
        chopsuey_check_failed(                                               \
            __FILE__, __LINE__, __func__, HEDLEY_STRINGIFY(expr));           \
    while (0)

HEDLEY_NO_RETURN
HEDLEY_NON_NULL(1, 3, 4)
void chopsuey_check_failed(
	char const *file,
	long line,
	char const *func,
	char const *expr);

/// @cond
HEDLEY_END_C_DECLS
/// @endcond

#endif /* CHOPSUEY_CHECK_H */
