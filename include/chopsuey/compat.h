/*
 * SPDX-FileCopyrightText: 2022 Oliver Ebert <oe@outputenable.net>
 *
 * SPDX-License-Identifier: Zlib
 */

/**
 ** @file
 ** C++ compatibility.
 **/

#ifndef CHOPSUEY_COMPAT_H
#define CHOPSUEY_COMPAT_H 1

#ifdef __cplusplus
#   define CHOPSUEY_LITERAL(s, t) t
#else
#   define CHOPSUEY_LITERAL(s, t) (s t)
#endif

#endif /* CHOPSUEY_COMPAT_H */
