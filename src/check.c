/*
 * SPDX-FileCopyrightText: © 2022 Oliver Ebert <oe@outputenable.net>
 *
 * SPDX-License-Identifier: Zlib
 */

/**
 ** @file
 ** Runtime checks.
 **/

#include <config.h>

#include <stdio.h>
#include <stdlib.h>

#if HAVE_ERR_H
#   include <err.h>
#endif

#include <chopsuey/check.h>

void
chopsuey_check_failed(
	char const *const file,
	long const line,
	char const *const func,
	char const *const expr)
	{
#if HAVE_WARNX
	// warnx(3) outputs "the last component of the program name, a colon
	// character, and a space", followed by our custom message.  The
	// output is terminated by a newline character.
	warnx(
		"%s:%ld: %s: Runtime check `%s' failed.",
		file,
		line,
		func,
		expr);

#elif HAVE___PROGNAME
	extern char const *__progname;
	(void)fprintf(
		stderr,
		"%s: %s:%ld: %s: Runtime check `%s' failed.",
		__progname,
		file,
		line,
		func,
		expr);
#else
#   error "Don't know how to determine program name!"
#endif
	(void)fflush(stderr);

	abort();
	}
