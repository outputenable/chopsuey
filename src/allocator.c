/*
 * SPDX-FileCopyrightText: 2021 Oliver Ebert <oe@outputenable.net>
 *
 * SPDX-License-Identifier: Zlib
 */

/**
 ** @file
 ** Allocator implementation.
 **/

#include <config.h>

#include "chopsuey/allocator.h"

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdint.h>
#include <stdlib.h>

HEDLEY_WARN_UNUSED_RESULT
HEDLEY_MALLOC
static void *
system_malloc(size_t const size, void *const ctx)
	{
	(void)ctx;
	return malloc(size);
	}

static void
system_free(void const *const ptr, void *const ctx)
	{
	(void)ctx;
	free((void *)ptr);
	}

static void *
system_calloc(size_t const nmemb, size_t const size, void *const ctx)
	{
	(void)ctx;
	return calloc(nmemb, size);
	}

static void *
system_realloc(void *const ptr, size_t const size, void *const ctx)
	{
	(void)ctx;
	return realloc(ptr, size);
	}

static void
system_alloc_ref(ChopsueyAllocator const alloc)
	{
	(void)alloc;
	}

static void
system_alloc_unref(ChopsueyAllocator const alloc)
	{
	(void)alloc;
	}

static struct ChopsueyAllocatorOps const
system_allocator_ops =
	{
	.malloc  = system_malloc,
	.free    = system_free,
	.calloc  = system_calloc,
	.realloc = system_realloc,
	.ref     = system_alloc_ref,
	.unref   = system_alloc_unref,
	};

ChopsueyAllocator
chopsuey_system_allocator(void)
	{
	return chopsuey_alloc_create(&system_allocator_ops, NULL);
	}

void *
chopsuey_overalloc_pow2(
	void *const ptr,
	size_t *const pnmemb,
	size_t const size,
	ChopsueyAllocator const alloc)
	{
	assert(size != 0);

	size_t new_nmemb = 8;
	size_t const min_nmemb = *pnmemb;
	if (new_nmemb < min_nmemb)
		{
		/*
		 * https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
		 */
		size_t v = min_nmemb - 1;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		HEDLEY_STATIC_ASSERT(sizeof (size_t) == SIZEOF_SIZE_T, "");
#if SIZEOF_SIZE_T * CHAR_BIT > 16
		v |= v >> 16;
#   if SIZEOF_SIZE_T * CHAR_BIT > 32
		v |= v >> 32;
#       if SIZEOF_SIZE_T * CHAR_BIT > 64
#           error "size_t > 64 bits!?"
#       endif
#   endif
#endif
		new_nmemb = v + 1;

		if (HEDLEY_UNLIKELY(new_nmemb == 0))
			new_nmemb = min_nmemb;
		}

	// Repeating the SIZE_MAX / size expression seems to generate better
	// code (at least with GCC 11.2 x86-64 2x mul+jo vs. 1x div+cmp+jb),
	// assuming that the first condition is likely false.
	if (HEDLEY_UNLIKELY(new_nmemb > SIZE_MAX / size)
		&& (new_nmemb = min_nmemb) > SIZE_MAX / size)
		{
		errno = ERANGE;
		return NULL;
		}

	void *new_ptr;
	while (HEDLEY_UNLIKELY(
		(new_ptr = chopsuey_realloc(ptr, new_nmemb * size, alloc))
		== NULL))
		if (new_nmemb != min_nmemb)
			new_nmemb = min_nmemb;
		else
			return NULL;

	*pnmemb = new_nmemb;
	return new_ptr;
	}
